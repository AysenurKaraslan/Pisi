module Pisi
  class Pisi

    def initialize
      @main_url = "http://ciftlik.pisilinux.org/2.0-stable/"
      $pisi_links = Array.new
      self.links

    end

    def links
      html_doc = Nokogiri::HTML(self.send_request(@main_url))
      links = Array.new
      html_doc = html_doc.css('a').each do |link|
        if link.attr('href').include? (".pisi")
          $pisi_links.push(link.attr('href'))
        elsif link.attr('href') != ("../")
            parent_link = @main_url + link.attr('href')
            links.push(parent_link)
        end
      end
      sublink_array = Array.new
      array = self.solution links, sublink_array
      pisi_array = Array.new
      pisi =  self.solution array, pisi_array

      pisi.each do |link|
        link = link.gsub("http://ciftlik.pisilinux.org/2.0-stable/","")
        $pisi_links.push(link)
      end

    end

    def solution links, sublink_array
      links.each do |link|
        html_doc = Nokogiri::HTML(self.send_request link)
        html_doc = html_doc.css('a').each do |sublink|
          if sublink.attr('href') != ("../")
            sublink = sublink.attr('href')
            new_link = link + sublink
            sublink_array.push(new_link)
          end
        end
      end
      sublink_array
    end

    def send_request link
      res = Typhoeus.get(link, followlocation: true)
      res.body
    end

  end
end
