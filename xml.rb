module Xml
  class Xml
    def initialize
      @main_url = "http://ciftlik.pisilinux.org/2.0-stable/pisi-index.xml"
      xml_doc = Nokogiri::XML(self.send_request(@main_url))
      $package_uris = Array.new
      xml_doc.css("Package/PackageURI").each do |uri|
        $package_uris.push(uri.text)
      end
    end

    def send_request link
      res = Typhoeus.get(link,followlocation: true)
      res.body
    end
  end
end
